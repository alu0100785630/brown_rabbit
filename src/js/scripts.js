$(document).ready(function(){

    //Ramdom Slider image
    var numImages = $('.slider img').length;
    var random = Math.floor(Math.random() * numImages);
    $(".slider img").eq(random).show(0);

    //Searcher Event
    $('#searcher').on("input", function(e){

        //TODO Find Posts

        //TODO Highlight Words
        var textHilighted = $(this).val();

        //TODO Getting al "posts"
        var posts = $('[name="posts"]');
        //Clear Highlighted Text
        posts.unhighlight();
        //Highlight Text
        posts.highlight(textHilighted, { caseSensitive: true });
    } );

    //Read More Event
    $('input[name="read-more"]').on("click",function(e){
      //TODO Hide Short Text
      var $this = $(this);
      $this.parent().find('.short-text').toggle();
      //TODO Show Long Text
      $this.parent().find('.long-text').toggle();
    });

  });
